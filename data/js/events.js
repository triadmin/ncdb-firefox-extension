var events = {};

// Handle Chrome alarms here
chrome.alarms.onAlarm.addListener( function(alarm)
{
  switch ( alarm.name )
  {
    case 'notifications':
      events.check_notifications();
      break;
  }
});

events.check_notifications = function() {
  var url = 'https://nationaldb.org/api/v1/users/user';
  
  // We don't have jQuery here. Gotta use XHR
  var xhr = new XMLHttpRequest();
  xhr.open('GET', url, true);
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4) {
      var json = JSON.parse(xhr.responseText);
      if ( typeof json.data.UsersNotificationCount != 'undefined' && json.data.UsersNotificationCount > 0 )
      {
        // Set icon badge (this should go in background.js file)
        chrome.browserAction.setBadgeBackgroundColor({ color: [255, 0, 0, 255] });
        chrome.browserAction.setBadgeText({text: json.data.UsersNotificationCount});
        
        // Display notification. For now, just display a general notification
        // for all messages received
        var notification_obj = {
          type: 'basic',
          title: 'NCDB Notifications',
          message: 'You have new notifications on the NCDB website.',
          iconUrl: '../images/icon48.png'
        }
        events.display_notification( notification_obj );
      } else {
        // Remove badge from extension icon.
        chrome.browserAction.setBadgeText({text: ''});
      }
    }
  }
  xhr.send();
}

events.display_notification = function( notification_obj )
{
  chrome.notifications.create('ncdbnotification', notification_obj, create_callback);
  chrome.notifications.onButtonClicked.addListener( btn_callback );
  
  function btn_callback() {
    chrome.tabs.create({ url: 'https://nationaldb.org?chrome' });
  }
  
  function create_callback() {
    // We could do something here when the notification displays
  }
}


