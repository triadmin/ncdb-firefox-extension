var { ToggleButton } = require('sdk/ui/button/toggle');
var panels = require('sdk/panel');
var self = require('sdk/self');
var data = self.data;
var tabs = require('sdk/tabs');
var { Cc, Ci }  = require('chrome');

var button = ToggleButton({
  id: "my-button",
  label: "my button",
  icon: {
    "16": "./icon16.png",
    "32": "./icon32.png",
    "64": "./icon64.png",
    "128": "./icon128.png"
  },
  onChange: handleChange
});

var panel = panels.Panel({
  width: 540,
  height: 430,
  contentURL: data.url('popup.html'),
  onHide: handleHide,
  contentScriptFile: [
    data.url('js/jquery-1.8.2.min.js'), 
    data.url('js/app.js'),
    data.url('js/bootstrap.min.js'),
    data.url('js/search.js')
  ]
});

function handleChange(state) {
  if (state.checked) {
    panel.port.emit('trylogin');
    panel.show({
      position: button
    });
  }
}

function handleHide() {
  button.state('window', {checked: false});
}

panel.port.on('newtab', function (url) {
  tabs.open( url );
  panel.hide();
});

function openTab( url )
{
  var tBrowser = top.document.getElementById('content');
  var tab = tBrowser.addTab(url + '?firefox');
  tBrowser.selectedTab = tab;
}